from __future__ import unicode_literals

from django.db import models

# Create your models here.

#dictionay category_id: category name
CATEGORY_LOOKUP = {0: 'cat1'}

class YoutubeItem:
    def __init__(self, vid, title):
        self.id = vid
        self.title = title

    def __repr__(self):
        return str(self.title)

    def __eq__(self, other):
        return other.id == self.id



class Video(YoutubeItem):
    def __init__(self, vid,title, description=None, view_count=None, like_count=None,\
                 dislike_count=None, comment_count=None, category_id=None, playlist_id=None, published_at=None,\
                 channel_id=None, audio_lang=None, tags=None):

        YoutubeItem.__init__(self,vid,title)
        self.description = description
        self.viewCount = view_count
        self.likeCount = like_count
        self.dislikeCount = dislike_count
        self.commentCount = comment_count
        self.favoriteCount = comment_count
        self.categoryId = category_id
        self.playlistId = playlist_id
        self.publishedAt = published_at
        self.channelId = channel_id
        self.audioLang = audio_lang
        self.tags = tags


class Playlist(YoutubeItem):
    def __init__(self, list_id, name):
        YoutubeItem.__init__(self,list_id, name)



class Channel(YoutubeItem):
    def __init__(self, channel_id, name):
        YoutubeItem.__init__(self,channel_id, name)

