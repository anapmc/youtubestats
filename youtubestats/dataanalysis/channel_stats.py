#!/usr/bin/python

# Usage example:

import httplib2
import os
from datetime import datetime,timedelta
import sys

from apiclient.discovery import build
from apiclient.errors import HttpError

from dataanalysis.models import Video
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow


YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

YOUTUBE_ANALYTICS_API_SERVICE_NAME = "youtubeAnalytics"
YOUTUBE_ANALYTICS_API_VERSION = "v1"

YOUTUBE_DATA_KEY = 'AIzaSyBrfjiv2PVr_gK3SW_lb38NGlwHAKWAdYA'
YOUTUBE_ANALYTICS_KEY = 'AIzaSyD3qV-nSDP142uPs162Zt-0T2MIV0gYhRI'

# Authorize the request and store authorization credentials.
def get_authenticated_service():

    return build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, developerKey=YOUTUBE_DATA_KEY)


def get_authenticated_analytics():
    return build(YOUTUBE_ANALYTICS_API_SERVICE_NAME,
                 YOUTUBE_ANALYTICS_API_VERSION, developerKey=YOUTUBE_ANALYTICS_KEY)


# Call the API's channels.list method to retrieve an existing channel id from the username.
def get_id_from_username(youtube, channel_un):
    results = youtube.channels().list(
            part="id",
            forUsername=channel_un
    ).execute()
    try:
        results["items"][0]["id"]
    except KeyError as (errno, strerror):
        print strerror
        return None

def get_channel_uploads_id(youtube,channel_un):
    results = youtube.channels().list(
            part="contentDetails",
            forUsername=channel_un
    ).execute()

    try:
        return results['items'][0]['contentDetails']['relatedPlaylists']['uploads']
    except IndexError:
        return None


def get_videos_from_playlist(youtube,playlistId):
    all_vids = []
    get_more = True
    next = ''
    while get_more:
        playlistitems = youtube.playlistItems().list(
                playlistId=playlistId,
                part='snippet',
                maxResults=50,
                pageToken=next,
        ).execute()
        try:
            all_vids = all_vids + [vid['snippet']['resourceId']['videoId'] for vid in playlistitems['items']]
            print len(all_vids)
        except:
            pass
        try:
            next = playlistitems['nextPageToken']
        except:
            get_more = False
    print len(all_vids)
    return all_vids
    # except IndexError as (err, e):
    #     print err, e
    #     return None


def get_video_info(youtube,videos_list):
    results = []
    for vid in videos_list:
        result = youtube.videos().list(
                part='snippet,statistics',
                id=vid
        ).execute()
        try:
            snippet = result['items'][0]['snippet']
            statistics = result['items'][0]['statistics']
            video = Video(vid, snippet['title'])
            video.description = snippet['description']
            video.categoryId = snippet['categoryId']
            video.channelId = snippet['channelId']
            # video.playlistId = snippet['playlistId']
            # video.audioLang = snippet['defaultAudioLanguage']
            video.tags = snippet['tags']
            video.publishedAt = snippet['publishedAt']
            video.viewCount = statistics['viewCount']
            video.commentCount = statistics['commentCount']
            video.likeCount = statistics['likeCount']
            video.dislikeCount = statistics['dislikeCount']
            video.favoriteCount = statistics['favoriteCount']
            results.append(video)
        except KeyError as key:
            #   #Check api for error handling
            print "something went wrong in vid, ", vid, "with key ", key
    return results


youtube = get_authenticated_service()
