from dataanalysis.channel_stats import youtube, get_videos_from_playlist, get_channel_uploads_id, get_video_info
from django.shortcuts import render

# Create your views here.
from django.template.context_processors import csrf


def home(request):
    ctx = {}
    if request.method == "POST":
        channel_un = request.POST['channel_username']
        playlistId = get_channel_uploads_id(youtube, channel_un)
        titles = []
        status = 'error'
        if playlistId:
            status = 'ok'
            vidIds = get_videos_from_playlist(youtube, playlistId)
            videos = get_video_info(youtube, vidIds)
            ctx = {
                'channel_name': channel_un,
                'status': status,
                'movies': videos
            }
        ctx.update(csrf(request))

    return render(request, 'base.html', ctx)

