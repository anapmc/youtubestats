/**
 * Created by anapmc on 14/02/16.
 */


likesList = Array('video_likes')
viewsList = Array('video')
likes_views_list = Array(['Likes', 'Views'])
channelName = ""

setChannelName = function(val){
    channelName = val
}

addToLikesList = function(val){
    likesList.push(val)
}

addToViewsList = function(val){
    viewsList.push(val)
}

addToAll = function(val){
    likes_views_list.push(val)
}