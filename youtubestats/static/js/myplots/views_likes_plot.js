/**
 * Created by anapmc on 14/02/16.
 */
document.addEventListener('DOMContentLoaded', function(){
    console.log('getting chart')

    google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable(likes_views_list);

        var options = {
          title: 'Age vs. Weight comparison',
          hAxis: {title: 'Age', minValue: 0, maxValue: 500},
          vAxis: {title: 'Weight', minValue: 0, maxValue: 500},
          legend: 'none'
        };

        var chart = new google.visualization.ScatterChart(document.getElementById('views_likes_chart'));

        chart.draw(data, options);
      }

    //view_likes_chart = c3.generate({
    //    bindto: '#views_likes_chart',
    //    data: {
    //        xs: {
    //            video: 'video_likes',
    //        },
    //        names: {
    //            video: channelName
    //        },
    //        columns: [
    //            likesList,
    //            viewsList
    //        ],
    //        type: 'scatter'
    //    },
    //    axis: {
    //        x: {
    //            label: 'Likes'
    //            //tick: { Check d3 formatting
    //            //    format: function (d) { return '$' + d; }
    //            //}
    //        },
    //        y: {
    //            label: 'Views'
    //            //tick: { Check d3 formatting
    //            //    format: function (d) { return '$' + d; }
    //            //}
    //        }
    //    }
    //});
    console.log('finishing... getting chart')

    document.getElementById('wait-upload').setAttribute('hidden','true')



});
